export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBsHPpJgkrhxbsbC7RpZzvBU_KgwrXNDXQ',
    authDomain: 'ng-myfitness-tracker.firebaseapp.com',
    databaseURL: 'https://ng-myfitness-tracker.firebaseio.com',
    projectId: 'ng-myfitness-tracker',
    storageBucket: 'ng-myfitness-tracker.appspot.com',
    messagingSenderId: '17612850345'
  }
};
