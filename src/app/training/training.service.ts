import {AngularFirestore} from '@angular/fire/firestore';
import {Injectable} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {Store} from '@ngrx/store';

import {Exercise} from './exercise.model';
import {UIServise} from '../shared/ui.service';
import * as UI from '../shared/ui.actions';
import * as fromRoot from '../app.reducer';
@Injectable()
export class TrainingService {
  exerciseChanged = new Subject<Exercise>();
  exercisesChanged = new Subject<Exercise[]>();
  finishedExChanged = new Subject<Exercise[]>();

  private availableExercises: Exercise[] = [];
  private runningExercise: Exercise;
  private fbSubs: Subscription[] = [];


  constructor(private db: AngularFirestore,
              private uiService: UIServise,
              private store: Store<fromRoot.State>) {}

  fetchAvailableExercises() {
    this.store.dispatch(new UI.StartLoading());
    this.fbSubs.push(this.db
      .collection('availableExercises')
      .snapshotChanges()
      .pipe(map(docArray => {
        // throw (new Error());
        return docArray.map(doc => {
          return {
            id: doc.payload.doc.id,
            // name: doc.payload.doc.data()['name'],
            // duration: doc.payload.doc.data()[duration],
            // calories: doc.payload.doc.data()[calories]
            ...doc.payload.doc.data()
          } as Exercise;
        });
      }))
        .subscribe((exercises: Exercise[]) => {
          this.store.dispatch(new UI.StopLoading());
          this.availableExercises = exercises;
          this.exercisesChanged.next([...this.availableExercises]);
        }, error => {
          this.store.dispatch(new UI.StopLoading());
          this.uiService.showSnackbar('Fetching Exercises failed, please try later', null, 3000);
          this.exercisesChanged.next(null);
        }));
  }

  startExercise(selectedId: string) {
    // this.db.doc('availableExercises/' + selectedId).update({lastSelected: new Date()});
    this.runningExercise  = this.availableExercises.find(ex => ex.id === selectedId);
    this.exerciseChanged.next({...this.runningExercise});
   }

   completeExercise() {
    this.addDataToDatabase({
      ...this.runningExercise,
      date: new Date(),
      state: 'completed'
    });
    this.runningExercise = null;
    this.exerciseChanged.next(null);
   }

   cancelExercise(progress: number) {
     this.addDataToDatabase({
       ...this.runningExercise,
       duration: this.runningExercise.duration * (progress / 100),
       calories: this.runningExercise.calories * (progress / 100),
       date: new Date(),
       state: 'cancelled'
     });
     this.runningExercise = null;
     this.exerciseChanged.next(null);

   }

   getRunningExercise() {
    return {...this.runningExercise};
   }

   fetchCompletedOrCanceledExercises() {
    this.fbSubs.push(this.db
      .collection('finished')
      .valueChanges()
      .subscribe(
      (exercises: Exercise[]) => {
        this.finishedExChanged.next(exercises);
      }
    ));
   }
   cancelSubscriptions() {
    this.fbSubs.forEach(sub => sub.unsubscribe());
   }

   private addDataToDatabase(exercise: Exercise) {
    this.db.collection('finished').add(exercise);

   }
}
